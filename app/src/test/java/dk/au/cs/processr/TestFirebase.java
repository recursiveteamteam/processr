package dk.au.cs.processr;

import android.test.AndroidTestCase;

import com.google.firebase.FirebaseApp;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.Utility;

/**
 * Created by Tobias Lund Petersen on 27-11-2017.
 */

public class TestFirebase extends AndroidTestCase {
    Utility util;

    @Override
    public void setUp() throws InterruptedException {
        FirebaseApp.initializeApp(mContext);
        util = new FirebaseUtil();
    }

    @Test
    public void testWrite()throws InterruptedException {
        Event e = new Event(new ArrayList<String>(),"test","test@test.dk","2019-08-17","2019-08-19","eastern road 7","London","UK");
    }
}
