package dk.au.cs.processr;

import android.content.Context;
import android.test.mock.MockContext;

import com.google.firebase.FirebaseApp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.Utility;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    Utility util;

    @Mock
    Context mMockContext;

    @Before
    public void setup(){
        mMockContext = new MockContext();
        FirebaseApp.initializeApp(mMockContext);
        util = new FirebaseUtil();
    }

    @Test
    public void sendPackageToFirebase(){
        Event e = new Event(new ArrayList<String>(),"test","test@test.dk","2019-08-17","2019-08-19","eastern road 7","London","UK");
    }
}