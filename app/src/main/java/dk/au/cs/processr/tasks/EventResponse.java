package dk.au.cs.processr.tasks;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public interface EventResponse {
    Object fromAsyncTask(Object o);
}
