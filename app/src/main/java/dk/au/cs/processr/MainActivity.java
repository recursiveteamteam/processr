package dk.au.cs.processr;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import dk.au.cs.processr.serializable.User;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

public class MainActivity extends AppCompatActivity implements UtilityResponse {

    public static final String TAG = "Processr";
    private SecurityUtil security;
    FirebaseUtil firebase;
    private NfcAdapter mNfcAdapter;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getColor(R.color.white));


        firebase = new FirebaseUtil();
        firebase.delegate = this;
        security = new SecurityUtil();
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchUpdatedFilterFromFirebase();
        updateUI();
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        if (!isNetworkAvailable()) {
            enableAll(false);
        } else {
            enableAll(true);
        }
        try {
            techDetected.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }

        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected, tagDetected, ndefDetected};

        mTechLists = new String[][]{new String[]{MifareClassic.class.getName()}};

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void enableAll(boolean active) {
        findViewById(R.id.button7).setEnabled(active);
        findViewById(R.id.search_button).setEnabled(active);
        findViewById(R.id.login_button).setClickable(active);
        findViewById(R.id.sign_up).setEnabled(active);
        findViewById(R.id.add_new_event).setEnabled(active);
        if(!active) {
            Toast.makeText(this, R.string.no_internet_toast, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            Log.d(TAG, "onNewIntent: " + intent.getAction());

            if (tag != null) {
                Toast.makeText(this, getString(R.string.message_tag_detected), Toast.LENGTH_SHORT).show();
                Ndef ndef = Ndef.get(tag);
                MifareClassic mfc = MifareClassic.get(tag);
                Log.d(TAG, "<NDEF READ> tag: " + tag.toString());
                Log.d(TAG, "<NDEF READ> Ndef different from null?: " + (ndef != null));
                Log.d(TAG, "<MIFARE READ> Mifare different from null?: " + (mfc != null));

                readFromNFC(mfc);
            }
        }
    }

    private void readFromNFC(MifareClassic mfc) {
        byte[] data;
        String eventId = "";
        String projectId = "";

        try {
            mfc.connect();
            Log.d(TAG, "<MIFARE READ> connected?");
            boolean auth = false;
            String cardData = null;
            int secCount = mfc.getSectorCount();
            int bCount = 0;
            int bIndex = 0;
            auth = mfc.authenticateSectorWithKeyA(1, MifareClassic.KEY_NFC_FORUM);
            Log.d(TAG, "<MIFARE> Authentication: " + auth + ", mfc: " + mfc.getBlockCount());
            Log.d(TAG, "<MIFARE> sectorcount: " + mfc.getSectorCount() + ", this sector: " + 1);
            if (auth) {
                bCount = mfc.getBlockCountInSector(1);
                bIndex = 0;
                for (int i = 0; i < bCount; i++) {
                    bIndex = mfc.sectorToBlock(1);
                    data = mfc.readBlock(bIndex);
                    Log.i(TAG, "<MIFARE READ> eventId" + getHexString(data, data.length));
                    eventId = getHexString(data,data.length);
                    bIndex++;
                }
            } else { // Authentication failed - Handle it
                Toast.makeText(this, "Authentication of chip failed.", Toast.LENGTH_SHORT).show();
            }
            mfc.close();
            mfc.connect();
            auth = mfc.authenticateSectorWithKeyA(2, MifareClassic.KEY_NFC_FORUM);
            Log.d(TAG, "<MIFARE> Authentication: " + auth + ", mfc: " + mfc.getBlockCount());
            Log.d(TAG, "<MIFARE> sectorcount: " + mfc.getSectorCount() + ", this sector: " + 1);
            if (auth) {
                bCount = mfc.getBlockCountInSector(2);
                bIndex = 0;
                for (int i = 0; i < bCount; i++) {
                    bIndex = mfc.sectorToBlock(2);
                    data = mfc.readBlock(bIndex);
                    Log.i(TAG, "<MIFARE READ> projectId" + getHexString(data, data.length));
                    projectId = getHexString(data,data.length);
                    bIndex++;
                }
            } else { // Authentication failed - Handle it
                Toast.makeText(this, "Authentication of chip failed.", Toast.LENGTH_SHORT).show();
            }
            mfc.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!eventId.equals("") && !projectId.equals("")){
            Intent intent = new Intent(this, ProjectActivity.class);
            intent.putExtra(getString(R.string.event_id), eventId);
            intent.putExtra(getString(R.string.project_id_text), projectId);
            startActivity(intent);
        }

    }

    private String getHexString(byte[] data, int length) {
        //return String.format("%040x",new BigInteger(length,data));

        return new String(data).replace("_","");
    }

    private void readFromNFC(Ndef ndef) {

        try {
            ndef.connect();

            Log.d(TAG, "<NDEF READ> Ndef-type: " + ndef.getType() + ", Ndef-msg: " + ndef.getNdefMessage());

            Log.d(TAG, "connected to NFC Ndef");
            NdefMessage ndefMessage = ndef.getNdefMessage();
            String message = new String(ndefMessage.getRecords()[0].getPayload());
            Log.d(TAG, "readFromNFC: " + message);
            String[] split = message.split(",");
            ndef.close();
            Intent intent = new Intent(this, ProjectActivity.class);
            intent.putExtra(getString(R.string.event_id), split[0]);
            intent.putExtra(getString(R.string.project_id_text), split[1]);
            startActivity(intent);

        } catch (IOException | FormatException e) {
            e.printStackTrace();

        }
    }

    private void updateUI() {
        FloatingActionButton addNewEvent = findViewById(R.id.add_new_event);
        Button signUpButton = findViewById(R.id.sign_up);
        Button login = findViewById(R.id.login_button);
        TextView welcomeBack = findViewById(R.id.welcome_back_text);
        if (getLoggedIn()) {
            signUpButton.setVisibility(View.GONE);
            addNewEvent.setVisibility(View.VISIBLE);
            login.setClickable(false);
            login.setVisibility(View.GONE);
            findViewById(R.id.log_out).setVisibility(View.VISIBLE);
            findViewById(R.id.log_out).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logOut();
                }
            });
            getUser();
        } else {
            signUpButton.setVisibility(View.VISIBLE);
            addNewEvent.setVisibility(View.GONE);
            welcomeBack.setVisibility(View.GONE);
            findViewById(R.id.log_out).setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
            login.setClickable(true);
        }
    }

    private void logOut() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(getString(R.string.saved_signOnFilter));
        editor.putBoolean(getString(R.string.signed_in), false);
        editor.apply();
        updateUI();

    }

    private void getUser() {
        firebase.fetchUser(FirebaseUtil.USER, (String) SecurityUtil.unpackSignOnFilter(getFilter()).get(SecurityUtil.USERID));
    }

    private void setWelcomeBack(User result) {
        TextView welcomeBack = findViewById(R.id.welcome_back_text);
        String text = String.format(getString(R.string.welcome_back), result.getName());
        welcomeBack.setText(text);
        welcomeBack.setVisibility(View.VISIBLE);
    }

    private boolean getLoggedIn() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        return pref.getBoolean(getString(R.string.signed_in), false);
    }

    private String getFilter() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        return pref.getString(getString(R.string.saved_signOnFilter), "");
    }


    private void fetchUpdatedFilterFromFirebase() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        String signOnFilter = pref.getString(getString(R.string.saved_signOnFilter), "");
        SharedPreferences.Editor editor = pref.edit();
        if (!signOnFilter.equals("")) {
            //TODO: Firebase doesn't store authorIds? Make it so, so that UserType matters
            firebase.fetchAndValidate(FirebaseUtil.UPDATE_FILTER, signOnFilter);
            editor.putBoolean(getString(R.string.signed_in), true);
        } else {
            editor.putBoolean(getString(R.string.signed_in), false);
        }
        editor.apply();
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if (returnMsg.equals(FirebaseUtil.UPDATE_FILTER)) updateFilter(result);
        if (returnMsg.equals(FirebaseUtil.USER)) setWelcomeBack((User) result);
    }

    private void updateFilter(Object result) {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        String updatedSignOnFilter = (String) result;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(getString(R.string.saved_signOnFilter), updatedSignOnFilter);
        editor.apply();
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void createNewUser(View view) {
        Intent intent = new Intent(this, CreateUserActivity.class);
        startActivity(intent);
    }

    public void showNearbyEvents(View view) {
        Intent intent = new Intent(this, EventListActivity.class);
        startActivity(intent);
    }

    public void showSearchedEvents(View view) {
        Intent intent = new Intent(this, EventListActivity.class);
        startActivity(intent);
    }

    public void createNewEvent(View view) {
        Intent intent = new Intent(this, CreateEventActivity.class);
        startActivity(intent);
    }
}
