package dk.au.cs.processr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class EventActivity extends AppCompatActivity implements UtilityResponse {

    private SecurityUtil security;
    FirebaseUtil firebase;
    private String eventId;
    private String eventName;
    Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventcards);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        firebase = new FirebaseUtil();
        firebase.delegate = this;
        security = new SecurityUtil();
        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.event_id))) {
            eventId = savedInstanceState.getString(getString(R.string.event_id));
        } else if (getIntent().getStringExtra(getString(R.string.event_id)) != null) {
            eventId = getIntent().getStringExtra(getString(R.string.event_id));
        } else {
            onEventLoadFailure();
        }


        findViewById(R.id.create_project_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateProjectActivity.class);
                intent.putExtra(getString(R.string.event_id), eventId);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (eventId != null && !eventId.equals("")) {
            clearViews();
            fetchProjectListFromFirebase(eventId);
            fetchEventInfo(eventId);
        }
    }

    private void clearViews() {
        LinearLayout projectSpinnerLayout = (LinearLayout) findViewById(R.id.project_spinner);
        projectSpinnerLayout.removeAllViews();
    }

    private void fetchEventInfo(String eventId) {
        firebase.fetchEvent(FirebaseUtil.EVENT, eventId);
    }

    private void onEventLoadFailure() {
        Log.d(MainActivity.TAG, "project load failed!");
        Toast.makeText(getBaseContext(), R.string.project_load_failure, Toast.LENGTH_LONG).show();
    }

    private void fetchProjectListFromFirebase(String eventId) {
        firebase.fetchProjects(FirebaseUtil.PROJECTS, eventId);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (eventId != null) {
            outState.putString(getString(R.string.event_id), eventId);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.event_id))) {
            eventId = savedInstanceState.getString(getString(R.string.event_id));
        }
    }

    private void updateUI(Map<String, Project> result) {
        LinearLayout projectSpinnerLayout = (LinearLayout) findViewById(R.id.project_spinner);
        if (result.keySet().size() > 0) {
            for (String id : result.keySet()) {
                Project p = result.get(id);
                findViewById(R.id.no_projects_found).setVisibility(View.GONE);
                CardView view = (CardView) LayoutInflater.from(this).inflate(R.layout.project_list_view, null);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setEnabled(false);
                        Intent intent = new Intent(getApplicationContext(), ProjectActivity.class);
                        intent.putExtra(getString(R.string.event_id), eventId);
                        intent.putExtra(getString(R.string.project_id_text), p.getProjectId());
                        startActivity(intent);
                    }
                });
                LinearLayout ll = (LinearLayout) view.getChildAt(0);
                TableLayout tl1 = (TableLayout) ll.getChildAt(0);
                ((TextView) ((TableRow) tl1.getChildAt(0)).getChildAt(0)).setText(p.getName());
                ((TextView) ((TableRow) tl1.getChildAt(0)).getChildAt(0)).setTextAppearance(R.style.TextAppearance_AppCompat_Headline);


                if (p.getTeam() != null) {
                    for (String name : p.getTeam()) {
                        TextView nameView = new TextView(getApplicationContext());
                        nameView.setText(name);
                        TableRow row = new TableRow(getApplicationContext());
                        row.addView(nameView);
                        tl1.addView(row);
                    }
                }


                projectSpinnerLayout.addView(view);
            }
        } else {
            findViewById(R.id.no_projects_found).setVisibility(View.VISIBLE);
        }
        if (!getLoggedIn()) {
            findViewById(R.id.create_project_button).setVisibility(View.GONE);
        } else {
            findViewById(R.id.create_project_button).setVisibility(View.VISIBLE);
        }
    }

    private boolean getLoggedIn() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        return pref.getBoolean(getString(R.string.signed_in), false);
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if (returnMsg.equals(FirebaseUtil.PROJECTS)) {
            updateUI((Map<String, Project>) result);
        }
        if (returnMsg.equals(FirebaseUtil.EVENT)) {
            Event e = (Event) result;
            eventName = e.getName();
            toolbar.setTitle(eventName);
            toolbar.setTitleTextColor(getColor(R.color.white));
        }
    }
}

