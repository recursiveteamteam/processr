package dk.au.cs.processr;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.fragments.NFCWriteFragment;
import dk.au.cs.processr.utility.PagerAdapter;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class ProjectActivity extends AppCompatActivity implements UtilityResponse {

    public static final String TAG = ProjectActivity.class.getSimpleName();


    private SecurityUtil security;
    FirebaseUtil firebase;
    private String eventId;
    private String projectId;
    private Button mBtWrite;
    private boolean isWrite = false;

    private NFCWriteFragment mNfcWriteFragment;
    private NfcAdapter mNfcAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projectview);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getColor(R.color.white));

        firebase = new FirebaseUtil();
        firebase.delegate = this;
        security = new SecurityUtil();
        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.event_id)) && savedInstanceState.containsKey(getString(R.string.project_id_text))) {
            eventId = savedInstanceState.getString(getString(R.string.event_id));
            projectId = savedInstanceState.getString(getString(R.string.project_id_text));
        } else if (getIntent().getStringExtra(getString(R.string.event_id)) != null && getIntent().getStringExtra(getString(R.string.project_id_text)) != null) {
            eventId = getIntent().getStringExtra(getString(R.string.event_id));
            projectId = getIntent().getStringExtra(getString(R.string.project_id_text));
        } else {
            onLoadProjectFailure();
        }

        setTabLayout();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mBtWrite = findViewById(R.id.nfc_write_button);
        mBtWrite.setOnClickListener(view -> showWriteFragment());
    }

    private void setTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.description));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.timeline));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.images));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(getColor(R.color.txtGrey), getColor(R.color.white));

        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        adapter.fillFragmentItems(eventId, projectId);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                adapter.fillFragmentItems(eventId, projectId, tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected, tagDetected, ndefDetected};

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        Log.d(TAG, "onNewIntent: " + intent.getAction());

        if (tag != null) {
            Toast.makeText(this, getString(R.string.message_tag_detected), Toast.LENGTH_SHORT).show();
            Ndef ndef = Ndef.get(tag);
            MifareClassic mfc = MifareClassic.get(tag);

            if (isWrite) {
                mNfcWriteFragment = (NFCWriteFragment) getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
                mNfcWriteFragment.onNfcDetected(mfc, eventId, projectId);
            }

        }
    }

    private void showWriteFragment() {
        isWrite = true;
        mNfcWriteFragment = (NFCWriteFragment) getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
        if (mNfcWriteFragment == null) {
            mNfcWriteFragment = NFCWriteFragment.newInstance();
        }
        mNfcWriteFragment.show(getFragmentManager(), NFCWriteFragment.TAG);
    }

    private void onLoadProjectFailure() {
        Log.d(MainActivity.TAG, "project load failed!");
        Toast.makeText(getBaseContext(), R.string.project_load_failure, Toast.LENGTH_LONG).show();
    }


    @Override
    public void getResponse(String returnMsg, Object result) {

    }
}
