package dk.au.cs.processr;

/**
 * Created by Tobias Lund Petersen on 29-11-2017.
 */

class ApplicationConstants {
    public static final String USER_PREFERENCES = "dk.au.cs.processr.userSettings";
    public static final String EVENT_PREFERENCES = "dk.au.cs.processr.eventSettings";
    public static final String PROJECT_PREFERENCES = "dk.au.cs.processr.projectSettings";
}
