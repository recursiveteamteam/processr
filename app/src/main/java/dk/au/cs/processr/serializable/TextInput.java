package dk.au.cs.processr.serializable;

/**
 * Created by Tobias Lund Petersen on 24-11-2017.
 */

public class TextInput {
    private String inputId;
    private String text;

    public TextInput() {
        //default datasnapshot constructor
    }

    public TextInput(String inputId, String text) {
        this.inputId = inputId;
        this.text = text;
    }

    public String getInputId() {
        return inputId;
    }

    public String getText() {
        return text;
    }



    @Override
    public String toString() {
        return "TextInput{" +
                "inputId='" + inputId + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
