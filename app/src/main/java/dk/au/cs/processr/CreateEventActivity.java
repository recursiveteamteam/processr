package dk.au.cs.processr;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class CreateEventActivity extends AppCompatActivity implements UtilityResponse {

    private FirebaseUtil firebase;
    EditText _name;
    String _authorId;
    EditText _street;
    EditText _city;
    EditText _country;
    Button _next;
    Button _submit;
    DatePicker _datePickerStart;
    DatePicker _datePickerEnd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createevent);
        firebase = new FirebaseUtil();
        firebase.delegate = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getColor(R.color.white));

        _name = findViewById(R.id.name);
        _street = findViewById(R.id.street);
        _city = findViewById(R.id.city);
        _country = findViewById(R.id.country);
        _next = findViewById(R.id.next_button);
        _submit = findViewById(R.id.submit_event_button);
        _datePickerStart = findViewById(R.id.datePicker_start);
        _datePickerEnd = findViewById(R.id.datePicker_end);
        setInitialUI();


    }

    private void setInitialUI() {
        _submit.setVisibility(View.GONE);
        findViewById(R.id.next_button_date).setVisibility(View.GONE);
        _datePickerStart.setVisibility(View.GONE);
        _datePickerEnd.setVisibility(View.GONE);
        _datePickerStart.setBackgroundColor(getColor(R.color.cardview_light_background));
        _datePickerEnd.setBackgroundColor(getColor(R.color.cardview_light_background));
        findViewById(R.id.datePicker_end_text).setVisibility(View.GONE);
        findViewById(R.id.datePicker_start_text).setVisibility(View.GONE);
    }

    private void setNextUI() {
        _next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndPickDate();
            }
        });
        _submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndPutEvent(v);
            }
        });
        findViewById(R.id.next_button_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndPickNextDate();
            }
        });
        _next.setVisibility(View.GONE);
        _submit.setVisibility(View.GONE);
        findViewById(R.id.next_button_date).setVisibility(View.VISIBLE);
        _datePickerStart.setVisibility(View.VISIBLE);
        _name.setVisibility(View.GONE);
        _street.setVisibility(View.GONE);
        _city.setVisibility(View.GONE);
        _country.setVisibility(View.GONE);
        findViewById(R.id.create_event_text).setVisibility(View.GONE);
        findViewById(R.id.datePicker_start_text).setVisibility(View.VISIBLE);
    }

    private void setFinalUI() {
        _submit.setVisibility(View.VISIBLE);
        findViewById(R.id.next_button_date).setVisibility(View.GONE);
        findViewById(R.id.datePicker_start_text).setVisibility(View.GONE);
        findViewById(R.id.datePicker_end_text).setVisibility(View.VISIBLE);
        _datePickerStart.setVisibility(View.GONE);
        _datePickerEnd.setVisibility(View.VISIBLE);
        _submit.setVisibility(View.VISIBLE);
        Date date = parseDate(_datePickerStart.getDayOfMonth(),_datePickerStart.getMonth(),_datePickerStart.getYear());
        _datePickerEnd.setMinDate(date.getTime());

    }

    private Date parseDate(int dayOfMonth, int month, int year) {
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
        Date start = new Date();
        try {
            start = parser.parse(""+year+"-"+(month+1)+"-"+dayOfMonth);
        } catch (ParseException e) {
            Log.e(MainActivity.TAG,"Couldn't parse date, " + e.getMessage());
        }

        return start;
    }

    private void validateAndPickDate() {
        if(!validateFirstInput()){
            onEventCreateFailed("Failed to create event");
            return;
        }

        setNextUI();
    }

    private void validateAndPickNextDate() {

        setFinalUI();
    }

    private void validateAndPutEvent(View v) {
        String name = _name.getText().toString();
        String street = _street.getText().toString();
        String city = _city.getText().toString();
        String country = _country.getText().toString();
        int startDate = _datePickerStart.getDayOfMonth();
        int startMonth = _datePickerStart.getMonth();
        int startYear = _datePickerStart.getYear();
        int endDate = _datePickerEnd.getDayOfMonth();
        int endMonth = _datePickerEnd.getMonth();
        int endYear = _datePickerEnd.getYear();

        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        String signOnFilter = pref.getString(getString(R.string.saved_signOnFilter),"");
        if(signOnFilter.equals("")){
            onEventCreateFailed("Create failed! you're not logged in!");
            return;
        }
        String userId = SecurityUtil.getUserIdFromSignOnFilter(signOnFilter);
        String startMonthString = String.valueOf(startMonth+1);
        String start = startYear+"-"+startMonthString+"-"+startDate;
        String endMonthString = String.valueOf(endMonth+1);

        String end = endYear+"-"+endMonthString+"-"+endDate;
        if(!name.equals("") && !start.equals("") && !end.equals("") && !street.equals("") && !city.equals("") && !country.equals("")) {
            Event e = new Event(new ArrayList<>(), name, userId, start, end, street, city, country);
            firebase.putEvent(e);
            Toast.makeText(this, "Great! The event '" + e.getName() + "' are now created", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    private void onEventCreateFailed(String s) {
        Log.d(MainActivity.TAG,"event create failed!");
        Toast.makeText(getBaseContext(),s, Toast.LENGTH_SHORT).show();
        _next.setEnabled(true);
    }

    private boolean validateFirstInput() {
        boolean valid = true;

        String name = _name.getText().toString();
        String street = _street.getText().toString();
        String city = _city.getText().toString();
        String country = _country.getText().toString();

        if(street.isEmpty()){
            _street.setError("enter valid streetname");
            valid = false;
        } else {
            _street.setError(null);
        }

        if(name.isEmpty()){
            _name.setError("enter a name");
            valid = false;
        } else {
            _name.setError(null);
        }

        if(city.isEmpty()){
            _city.setError("enter valid city");
            valid = false;
        } else {
            _city.setError(null);
        }

        if(country.isEmpty()){
            _country.setError("enter valid country");
            valid = false;
        } else {
            _country.setError(null);
        }

        return valid;
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        // no response, but it communicates to firebase
    }
}
