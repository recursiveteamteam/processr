package dk.au.cs.processr.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import dk.au.cs.processr.R;

public class NFCWriteFragment extends DialogFragment {

    public static final String TAG = NFCWriteFragment.class.getSimpleName();

    public static NFCWriteFragment newInstance() {

        return new NFCWriteFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onNfcDetected(MifareClassic mfc, String eventId, String projectId) {
        writeToNfc(mfc, eventId, projectId);
    }

    private void writeToNfc(MifareClassic mfc, String eventId, String projectId) {
        Log.d(TAG, "<MIFARE WRITE> !");
        if (mfc != null) {
            try {
                // connect to MifareClassic tag
                mfc.connect();
                Log.d(TAG, "<MIFARE WRITE> Mfc: " + mfc.getBlockCount());
                // Authenticate our NFC-connection (3-step)
                boolean authA = mfc.authenticateSectorWithKeyA(1, MifareClassic.KEY_NFC_FORUM);

                Log.d(TAG, "<MIFARE WRITE> mfc authenticated?: " + authA);
                int l1 = eventId.length();
                for (int i = 0; i < 16 - l1; i++) {
                    eventId += '_';
                }
                int l2 = projectId.length();
                for (int i = 0; i < 16 - l2; i++) {
                    projectId += '_';
                }
                byte[] data1 = eventId.getBytes(StandardCharsets.UTF_8);
                byte[] data2 = projectId.getBytes(StandardCharsets.UTF_8);
                Log.d(TAG, "<MIFARE WRITE> 1 bytes repres: " + data1.length + ", msg: " + eventId);
                Log.d(TAG, "<MIFARE WRITE> 2 bytes repres: " + data2.length + ", msg: " + projectId);

                mfc.writeBlock(mfc.sectorToBlock(1), data1);
                Log.d(TAG, "<MIFARE READ/WRITE> wrote: " + mfc.readBlock(mfc.sectorToBlock(1)) + " which is " + new String(mfc.readBlock(mfc.sectorToBlock(1))));
                mfc.close();
                // We need to close and open connection again, apparently
                mfc.connect();
                boolean authB = mfc.authenticateSectorWithKeyA(2, MifareClassic.KEY_NFC_FORUM);

                mfc.writeBlock(mfc.sectorToBlock(2), data2);
                //mfc.transceive(data1);
                Log.d(TAG, "<MIFARE WRITE> wrote to block?");
                Log.d(TAG, "<MIFARE READ/WRITE> wrote: " + mfc.readBlock(mfc.sectorToBlock(2)) + " which is " + new String(mfc.readBlock(mfc.sectorToBlock(2))));
                mfc.close();
                //mfc.writeBlock(mfc.sectorToBlock(2),data2);
                Toast.makeText(getContext(), getText(R.string.successful_nfc_push), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}