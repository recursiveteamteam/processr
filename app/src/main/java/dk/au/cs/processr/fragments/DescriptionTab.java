package dk.au.cs.processr.fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.LinearLayoutCompat;
import android.widget.LinearLayout;
import android.widget.TextView;

import dk.au.cs.processr.R;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.TabFragment;
import dk.au.cs.processr.utility.UtilityResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionTab extends android.support.v4.app.Fragment implements TabFragment, UtilityResponse {

    private FirebaseUtil firebase;

    public DescriptionTab() {

        firebase = new FirebaseUtil();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab_fragment1, container, false);
        return view;
    }

    @Override
    public void setContent(String eventId, String projectId){
        firebase.delegate = this;
        firebase.fetchProject(FirebaseUtil.PROJECT,eventId,projectId);
    }

    @Override
    public void deleteContent() {
        deleteUI();

    }

    private void deleteUI() {
        View view = getView();
        ((LinearLayout)view.findViewById(R.id.participants)).removeAllViews();
        ((LinearLayout)view.findViewById(R.id.keywords)).removeAllViews();
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if(returnMsg.equals(FirebaseUtil.PROJECT)){
            updateUI((Project)result);
        }
    }

    private void updateUI(Project result) {

        View view = getView();
        TextView headline = view.findViewById(R.id.project_headline);
        headline.setText(result.getName());
        TextView description = view.findViewById(R.id.project_desc);
        description.setText(result.getDescription());

        LinearLayout participiants = (LinearLayout)view.findViewById(R.id.participants);
        for (String member: result.getTeam()) {
            TextView text = new TextView(this.getContext());
            text.setTextColor(getResources().getColor(R.color.white));
            text.setText(member);
            participiants.addView(text);
        }

        LinearLayout keywords= view.findViewById(R.id.keywords);
        for (String keyword: result.getKeywords()) {
            TextView text = new TextView(this.getContext());
            text.setText(keyword);
            keywords.addView(text);
        }
    }
}
