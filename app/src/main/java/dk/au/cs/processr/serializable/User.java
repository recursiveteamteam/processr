package dk.au.cs.processr.serializable;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class User {
    private String name;
    private String email;
    private String signOnFilter;

    public User() {
        //default datasnapshot constructor
    }

    public User(String name, String email, String signOnFilter) {
        this.name = name;
        this.email = email;
        this.signOnFilter = signOnFilter;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSignOnFilter(String signOnFilter) {
        this.signOnFilter = signOnFilter;
    }

    public String getName() {
        return name;
    }

    public String getUserId(){
        return "200" + String.valueOf(hashCode());
    }

    public String getEmail() {
        return email;
    }

    public String getSignOnFilter() {
        return signOnFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if(email != null ? !email.equals(user.email) : user.email != null) return false;
        return getUserId().equals(user.getUserId());
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", userId='" + getUserId() + '\'' +
                ", signOnFilter='" + signOnFilter + '\'' +
                '}';
    }
}
