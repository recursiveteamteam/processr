package dk.au.cs.processr;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dk.au.cs.processr.serializable.User;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class CreateUserActivity extends AppCompatActivity implements UtilityResponse {

    private FirebaseUtil firebase;
    EditText _name;
    EditText _email;
    Button _signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        firebase = new FirebaseUtil();

        _name = findViewById(R.id.name);
        _email = findViewById(R.id.street);
        _signup = findViewById(R.id.signup_button);

        _signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewUser(v);
            }
        });
    }

    public void onSignUpFailed(String failMsg) {
        Log.d(MainActivity.TAG,"Login failed!");
        Toast.makeText(getBaseContext(),failMsg, Toast.LENGTH_SHORT).show();
        _signup.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String name = _name.getText().toString();
        String email = _email.getText().toString();

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            _email.setError("enter valid email address");
            valid = false;
        } else {
            _email.setError(null);
        }

        if(name.isEmpty()){
            _name.setError("enter a name");
            valid = false;
        } else {
            _name.setError(null);
        }

        return valid;
    }

    private void updateSharedPreferences(User user){
        SharedPreferences prefs = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(user != null || !user.getName().equals("") || !user.getEmail().equals("")) {
            editor.putString("signOnFilter", user.getSignOnFilter());
            editor.apply();
            Log.d(MainActivity.TAG,"Shared preferences updated with new signOnFilter");
            onSignupSuccess();
        } else {
            onSignUpFailed("signup failed - wrong input");
        }
        Toast.makeText(this, "Welcome! You're now a user", Toast.LENGTH_LONG).show();
    }

    private void onSignupSuccess() {
        _signup.setEnabled(true);
        setResult(RESULT_OK,null);
        finish();
    }

    public void createNewUser(View view){
        String name =((EditText)findViewById(R.id.name)).getText().toString();
        String email = ((EditText)findViewById(R.id.street)).getText().toString();
        if(!validate()){
            onSignUpFailed("Login failed - could not validate input");
            return;
        }

        _signup.setEnabled(false);

        User user = new User(name,email,null);
        String signOnFilter = SecurityUtil.buildSignOnFilter(user.getUserId());
        user.setSignOnFilter(signOnFilter);
        firebase.putUser(user);
        updateSharedPreferences(user);
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if (result instanceof User) {
            updateSharedPreferences((User)result);
        }
    }
}
