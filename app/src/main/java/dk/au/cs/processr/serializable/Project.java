package dk.au.cs.processr.serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class Project {
    private String name;
    private String description;
    private List<String> keywords;
    private List<String> team;
    private HashMap<String,String> timeline;
    private List<String> images;

    public Project() {
        //default datasnapshot constructor
    }

    /**
     *
     * @param name
     * @param description
     * @param team map with userid and username
     * @param keywords list with keywords
     * @param timeline map with date/time and corresponding text-input id.
     * @param images list with references to images
     */
    public Project(String name, String description, List<String> team, List<String> keywords, HashMap<String, String> timeline, List<String> images) {
        this.name = name;
        this.description = description;
        this.team = team;
        this.keywords = keywords;
        this.timeline = timeline;
        this.images = images;
    }

    public String getProjectId() {
        return "300" + hashCode();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTeam() {
        return team;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public HashMap<String, String> getTimeline() {
        return timeline;
    }

    public List<String> getImages() {
        return images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (name != null ? !name.equals(project.name) : project.name != null) return false;
        if (keywords != null ? !keywords.equals(project.keywords) : project.keywords != null)
            return false;
        return team != null ? team.equals(project.team) : project.team == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (keywords != null ? keywords.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", keywords=" + keywords +
                ", team=" + team +
                ", timeline=" + timeline +
                ", images=" + images +
                '}';
    }
}
