package dk.au.cs.processr.utility;

import android.content.SharedPreferences;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.serializable.Project;

/**
 * Created by Tobias Lund Petersen on 24-11-2017.
 */

public class SecurityUtil implements Utility {

    public static final String USER = "user", EVENT_OWNER = "event_owner", PROJECT_OWNER = "project_owner", ADMIN = "admin";
    public static final String USERTYPE = "userType", USERID = "userId";

    /**
     * Builds a JSONObject with these params:
     * 'hasEventToken': has event access
     * 'hasProjectToken':
     *
     * @param userId
     * @return
     */
    public static String buildSignOnFilter(String userId) {
        String json = "";
        try {
            json = new JSONObject().put("userType", USER)
                    .put("events",null)
                    .put("projects",null)
                    .put("userId", userId).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static Map<String, Object> unpackSignOnFilter(String signOnFilter) {
        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Map<String, Object> signOn = gson.fromJson(signOnFilter, type);

        return signOn;
    }

    public static String packSignOnFilter(Map<String, Object> signOnFilter) {
        Gson gson = new Gson();
        String filter = gson.toJson(signOnFilter);
        return filter;
    }

    public static String getUserIdFromSignOnFilter(String signOnFilter){
        Map<String,Object> unpacked = unpackSignOnFilter(signOnFilter);
        return (String)unpacked.get(3);
    }

    public static String validateProjectOwner(String signOnFilter, HashMap<String, Project> projects) {
        Map<String, Object> signOnMap = unpackSignOnFilter(signOnFilter);
        for (String id : projects.keySet()) {
            for (String teamId : projects.get(id).getTeam()) {
                if (teamId.equals(signOnMap.get(USERID))) {
                    signOnMap.put(USERTYPE,PROJECT_OWNER);
                    Object added = signOnMap.get(FirebaseUtil.PROJECTS);
                    if(added == null) {
                        signOnMap.put(FirebaseUtil.PROJECTS, new ArrayList<String>().add(id));
                    } else {
                        signOnMap.put(FirebaseUtil.PROJECTS,((ArrayList<String>)added).add(id));
                    }
                }
            }
        }
        return packSignOnFilter(signOnMap);
    }

    public static String validateEventOwner(String signOnFilter, HashMap<String, Event> events) {
        Map<String, Object> signOnMap = unpackSignOnFilter(signOnFilter);
        for (String id : events.keySet()) {
            if (events.get(id).getauthorId().equals(signOnMap.get(USERID))) {
                signOnMap.put(USERTYPE, EVENT_OWNER);
                Object added = signOnMap.get(FirebaseUtil.EVENTS);
                if(added == null) {
                    signOnMap.put(FirebaseUtil.EVENTS, new ArrayList<String>().add(id));
                } else if(!((ArrayList<String>)added).contains(id)) {
                    signOnMap.put(FirebaseUtil.EVENTS,((ArrayList<String>)added).add(id));
                }
            }
        }
        return packSignOnFilter(signOnMap);
    }






    @Override
    public void execute(String command) {

    }
}
