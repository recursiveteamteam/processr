package dk.au.cs.processr.serializable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class Event {

    @SerializedName("country")
    private String country;
    @SerializedName("name")
    private String name;
    @SerializedName("projects")
    private List<String> projects;
    @SerializedName("author_id")
    private String authorId;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("address")
    private String address;
    @SerializedName("city")
    private String city;

    public Event() {
        //default datasnapshot constructor
    }

    public Event(List<String> projects, String name, String authorId, String startDate, String endDate, String address, String city, String country) {
        this.projects = projects;
        this.name = name;
        this.authorId = authorId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.address = address;
        this.city = city;
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProjects(List<String> projects) {
        this.projects = projects;
    }

    public void setEventId(String eventId){}

    public void setauthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEventId() {
        return "100" + hashCode();
    }

    public List<String> getProjects() {
        return projects;
    }

    public String getName() {
        return name;
    }

    public String getauthorId() {
        return authorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (country != null ? !country.equals(event.country) : event.country != null) return false;
        if (name != null ? !name.equals(event.name) : event.name != null) return false;
        if (projects != null ? !projects.equals(event.projects) : event.projects != null)
            return false;
        if (authorId != null ? !authorId.equals(event.authorId) : event.authorId != null)
            return false;
        if (startDate != null ? !startDate.equals(event.startDate) : event.startDate != null)
            return false;
        if (endDate != null ? !endDate.equals(event.endDate) : event.endDate != null) return false;
        if (address != null ? !address.equals(event.address) : event.address != null) return false;
        return city != null ? city.equals(event.city) : event.city == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (projects != null ? projects.hashCode() : 0);
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", projects=" + projects +
                ", authorId='" + authorId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", address='" + address + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
