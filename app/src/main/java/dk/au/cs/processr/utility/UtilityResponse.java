package dk.au.cs.processr.utility;

import java.lang.reflect.Type;

/**
 * Created by Tobias Lund Petersen on 29-11-2017.
 */

public interface UtilityResponse {

    /**
     * Get the response from firebase
     * @param returnMsg
     * @param result
     */
    void getResponse(String returnMsg, Object result);
}
