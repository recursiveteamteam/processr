package dk.au.cs.processr.tasks;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by Tobias Lund Petersen on 29-11-2017.
 */

public abstract class ChildFetchFromFirebaseListener implements ChildEventListener {

    @Override
    public abstract void onChildAdded(DataSnapshot dataSnapshot, String s);

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
