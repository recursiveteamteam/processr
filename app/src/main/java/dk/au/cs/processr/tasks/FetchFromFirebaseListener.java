package dk.au.cs.processr.tasks;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Type;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public abstract class FetchFromFirebaseListener implements ValueEventListener {

    public FetchFromFirebaseListener(){}

    @Override
    public abstract void onDataChange(DataSnapshot dataSnapshot);

    @Override
    public void onCancelled(DatabaseError databaseError) {
        System.out.println("The read failed: " + databaseError.getCode());
        System.out.println("Details: " + databaseError.getMessage());
    }
}
