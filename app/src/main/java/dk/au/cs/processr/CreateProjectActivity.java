package dk.au.cs.processr;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 06-12-2017.
 */

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class CreateProjectActivity extends AppCompatActivity implements UtilityResponse {

    private FirebaseUtil firebase;
    EditText _name;
    EditText _description;
    EditText _keywords;
    EditText _team;
    String eventId;
    Button _submit;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createproject);
        firebase = new FirebaseUtil();
        firebase.delegate = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getColor(R.color.white));

        _name = findViewById(R.id.name);
        _description = findViewById(R.id.description);
        _keywords = findViewById(R.id.keywords);
        _team = findViewById(R.id.team_project);
        _submit = findViewById(R.id.submit_project_button);

        _submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndPutProject(v);
            }
        });

        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.event_id))) {
            eventId = savedInstanceState.getString(getString(R.string.event_id));
        } else if (getIntent().getStringExtra(getString(R.string.event_id)) != null) {
            eventId = getIntent().getStringExtra(getString(R.string.event_id));
        } else {
            onProjectCreateFailed(getString(R.string.no_projectid_given));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (eventId != null) {
            outState.putString(getString(R.string.event_id), eventId);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.event_id))) {
            eventId = savedInstanceState.getString(getString(R.string.event_id));
        }
    }


    private void validateAndPutProject(View v) {
        String name = _name.getText().toString();
        String description = _description.getText().toString();
        List<String> keywords = new ArrayList<String>(Arrays.asList(_keywords.getText().toString().split(",")));
        List<String> team = new ArrayList<String>(Arrays.asList(_team.getText().toString().split(",")));

        if(!validate()){
            onProjectCreateFailed(getString(R.string.project_create_failed_short_toast));
            return;
        }

        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        String signOnFilter = pref.getString(getString(R.string.saved_signOnFilter), "");
        if (signOnFilter.equals("")) {
            onProjectCreateFailed(getString(R.string.create_project_failed_toast));
            return;
        }

        if (!name.equals("") && !description.equals("") && keywords.size()>0 && team.size()>0 && eventId != null) {
            Project p = new Project(name,description,team,keywords,null,null);
            firebase.putProject(p,eventId);
            Toast.makeText(this, "Great! The project '" + p.getName() + "' are now created", Toast.LENGTH_LONG).show();
        }
        finish();
    }

    private void onProjectCreateFailed(String s) {
        Log.d(MainActivity.TAG, getString(R.string.project_create_failed_short_toast));
        Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
        _submit.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String name = _name.getText().toString();
        String description = _description.getText().toString();
        String keywords = _keywords.getText().toString();
        String team = _team.getText().toString();

        if (name.isEmpty()) {
            _name.setError(getString(R.string.enter_valid_projectname));
            valid = false;
        } else {
            _name.setError(null);
        }

        if (description.isEmpty()) {
            _description.setError(getString(R.string.enter_valid_description));
            valid = false;
        } else {
            _description.setError(null);
        }

        if (keywords.isEmpty() || keywords.split(",").length==0) {
            _keywords.setError(getString(R.string.enter_keywords));
            valid = false;
        } else {
            _keywords.setError(null);
        }

        if (team.isEmpty() || keywords.split(",").length==0) {
            _team.setError(getString(R.string.enter_members_split));
            valid = false;
        } else {
            _team.setError(null);
        }

        return valid;
    }

    @Override
    public void getResponse(String returnMsg, Object result) {

    }
}


