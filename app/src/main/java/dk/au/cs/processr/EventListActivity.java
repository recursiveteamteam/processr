package dk.au.cs.processr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class EventListActivity extends AppCompatActivity implements UtilityResponse {


    private SecurityUtil security;
    FirebaseUtil firebase;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.events);
        toolbar.setTitleTextColor(getColor(R.color.white));

        firebase = new FirebaseUtil();
        firebase.delegate = this;
        security = new SecurityUtil();
    }

    @Override
    protected void onResume() {
        super.onResume();
        clearView();
        fetchEventListFromFirebase();
    }

    private void clearView() {
        LinearLayout eventSpinnerLayout = (LinearLayout) findViewById(R.id.event_spinner);
        eventSpinnerLayout.removeAllViews();
    }

    private void fetchEventListFromFirebase() {
        firebase.fetchEvents(FirebaseUtil.EVENTS);
    }

    private void updateUI(Map<String, Event> result) {
        LinearLayout eventSpinnerLayout = (LinearLayout) findViewById(R.id.event_spinner);

        for (String id : result.keySet()) {
            Event e = result.get(id);
            LinearLayout view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.event_list_view, null);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setEnabled(false);
                    Intent intent = new Intent(getApplicationContext(), EventActivity.class);
                    intent.putExtra(getString(R.string.event_id), id);
                    startActivity(intent);
                }
            });
            ((FloatingActionButton) view.findViewById(R.id.floating_image)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setEnabled(false);
                    Intent intent = new Intent(getApplicationContext(), EventActivity.class);
                    intent.putExtra(getString(R.string.event_id), id);
                    startActivity(intent);
                }
            });
            ((TextView) ((LinearLayout) view.getChildAt(1)).getChildAt(0)).setText(e.getName());
            ((TextView) ((LinearLayout) view.getChildAt(1)).getChildAt(1)).setText(calculateDate(e.getStartDate(), e.getEndDate()));
            ((TextView) view.getChildAt(2)).setText(new StringBuilder().append(e.getCity()).append("\n").append(e.getCountry()).toString());
            eventSpinnerLayout.addView(view);
        }
        if (!getLoggedIn()) {
            findViewById(R.id.create_event_button).setVisibility(View.GONE);
        } else {
            findViewById(R.id.create_event_button).setVisibility(View.VISIBLE);
        }

    }

    private boolean getLoggedIn() {
        SharedPreferences pref = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        return pref.getBoolean(getString(R.string.signed_in), false);
    }

    public void createNewEvent(View view) {
        Intent intent = new Intent(this, CreateEventActivity.class);
        startActivity(intent);
    }

    private String calculateDate(String startDate, String endDate) {
        SimpleDateFormat parser = new SimpleDateFormat(getString(R.string.date_input_format), Locale.UK);
        Date start = new Date();
        Date end = new Date();
        try {
            start = parser.parse(startDate);
            end = parser.parse(endDate);
        } catch (ParseException e) {
            Log.e(MainActivity.TAG, "Couldn't parse date, " + e.getMessage());
        }
        SimpleDateFormat formatter = new SimpleDateFormat(getString(R.string.date_output_format), Locale.UK);
        String _startDate = formatter.format(start);
        String _endDate = formatter.format(end);
        return _startDate + " to " + _endDate;
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if (returnMsg.equals(FirebaseUtil.EVENTS)) {
            updateUI((HashMap<String, Event>) result);
        }
    }
}
