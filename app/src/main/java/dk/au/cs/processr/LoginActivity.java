package dk.au.cs.processr;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;

import dk.au.cs.processr.serializable.User;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.SecurityUtil;
import dk.au.cs.processr.utility.UtilityResponse;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class LoginActivity extends AppCompatActivity implements UtilityResponse {

    FirebaseUtil firebase;
    EditText _name;
    EditText _email;
    Button _login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebase = new FirebaseUtil();
        firebase.delegate = this;

        _name = findViewById(R.id.name);
        _email = findViewById(R.id.street);
        _login = findViewById(R.id.signup_button);

        _login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logUserIn(v);
            }
        });
    }

    public void logUserIn(View view){
        Log.d(MainActivity.TAG,"Login");
        if(!validate()){
            onLoginFailed("Login failed - could not validate input");
            return;
        }

        _login.setEnabled(false);

        String name = _name.getText().toString();
        String email = _email.getText().toString();

        firebase.fetchUser(FirebaseUtil.USER,name,email);
    }

    public void onLoginFailed(String failMsg) {
        Log.d(MainActivity.TAG,"Login failed!");
        Toast.makeText(getBaseContext(),failMsg, Toast.LENGTH_SHORT).show();
        _login.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String name = _name.getText().toString();
        String email = _email.getText().toString();

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            _email.setError("enter valid email address");
            valid = false;
        } else {
            _email.setError(null);
        }

        if(name.isEmpty()){
            _name.setError("enter a name");
            valid = false;
        } else {
            _name.setError(null);
        }

        return valid;
    }

    private void updateSharedPreferences(User user){
        SharedPreferences prefs = getSharedPreferences(ApplicationConstants.USER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(user != null || !user.getName().equals("") || !user.getEmail().equals("")) {
            editor.putString(getString(R.string.saved_signOnFilter), user.getSignOnFilter());
            editor.apply();
            Log.d(MainActivity.TAG,"Shared preferences updated with new signOnFilter");
            onLoginSuccess();
        } else {
            onLoginFailed("Login failed - could not find user");
        }
    }

    private void onLoginSuccess() {
        _login.setEnabled(true);
        setResult(RESULT_OK,null);
        finish();
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if (returnMsg.equals(FirebaseUtil.USER)) {
            updateSharedPreferences((User)result);
        }
    }
}
