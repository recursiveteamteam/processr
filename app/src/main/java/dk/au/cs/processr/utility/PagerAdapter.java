package dk.au.cs.processr.utility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dk.au.cs.processr.fragments.DescriptionTab;
import dk.au.cs.processr.fragments.TimelineTab;
import dk.au.cs.processr.fragments.ImageTab;

/**
 * Created by Jesper on 06-12-2017.
 * borrowed most of this from googles eveloper training.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    private DescriptionTab descriptionTab;
    private TimelineTab timelineFragment;
    private ImageTab imageFragment;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        descriptionTab = new DescriptionTab();
        timelineFragment = new TimelineTab();
        imageFragment = new ImageTab();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return descriptionTab;
            case 1:
                return timelineFragment;
            case 2:
                return imageFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public void fillFragmentItems(String eventId, String projectId) {
        
        descriptionTab.setContent(eventId, projectId);
        timelineFragment.setContent(eventId, projectId);
        imageFragment.setContent(eventId, projectId);
    }

    private void cleanContent(TabFragment frag){
        frag.deleteContent();
    }

    public void fillFragmentItems(String eventId, String projectId, int pos) {
        cleanContent((TabFragment)getItem(pos));
        ((TabFragment)getItem(pos)).setContent(eventId,projectId);
    }
}