package dk.au.cs.processr.utility;

/**
 * Created by Jesper on 07-12-2017.
 */

public interface TabFragment {

    void setContent(String eventId, String projectId);

    void deleteContent();
}
