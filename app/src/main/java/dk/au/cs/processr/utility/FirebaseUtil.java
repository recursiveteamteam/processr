package dk.au.cs.processr.utility;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.au.cs.processr.MainActivity;
import dk.au.cs.processr.serializable.Event;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.serializable.User;
import dk.au.cs.processr.tasks.FetchFromFirebaseListener;

/**
 * Created by Tobias Lund Petersen on 23-11-2017.
 */

public class FirebaseUtil implements Utility {

    public static final String NO_USER_FOUND = "no_user_found",
            USER = "user", PROJECTS = "projects", PROJECT = "project",
            EVENTS = "events", EVENT = "event",
            GUEST_LIST = "guest_list", UPDATE_FILTER = "update_filter";

    FirebaseDatabase database;
    public UtilityResponse delegate = null;

    public FirebaseUtil() {
        database = FirebaseDatabase.getInstance();
    }

    private void deleteFromGuestList(List<String> guestIds, String eventId) {
        DatabaseReference ref = database.getReference("guest_lists");
        for (String id : guestIds) {
            ref.child(eventId).child(id).removeValue();
        }
    }

    public void putGuestList(List<String> guestList, String eventId) {
        DatabaseReference ref = database.getReference("guest_lists");
        ref.child(eventId).setValue(guestList);
        Log.d(MainActivity.TAG,"<FIREBASE> PUT Guest list for " + eventId + " with " + guestList.size() + " entries");
    }

    public void putUser(User user) {
        DatabaseReference ref = database.getReference("users");
        ref.child(user.getUserId()).setValue(user);
        Log.d(MainActivity.TAG,"<FIREBASE> PUT User " + user.getName() + ":" + user.getUserId());
    }

    /**
     * Put single project
     *
     * @param project
     * @param eventId
     */
    public void putProject(Project project, String eventId) {
        DatabaseReference ref = database.getReference("projects");
        ref.child(eventId).child(project.getProjectId()).setValue(project);
        Log.d(MainActivity.TAG,"<FIREBASE> PUT Project " + project.getName() + ":" + project.getProjectId() + " for " + eventId);
    }

    /**
     * Put single event
     *
     * @param event
     */
    public void putEvent(Event event) {
        DatabaseReference ref = database.getReference("events");
        ref.child(event.getEventId()).setValue(event);
        Log.d(MainActivity.TAG,"<FIREBASE> PUT Event " + event.getName() + ":" + event.getEventId());

    }

    /**
     * Fetch user with user id
     *
     * @param userId
     */
    public void fetchUser(String respMsg, String userId) {
        DatabaseReference ref = database.getReference("users");
        ref.child(userId).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH user " + user.getName() + ":" + userId);
                delegate.getResponse(respMsg, user);
            }
        });
    }

    /**
     * Fetch user with name and email (activity_login)
     *
     * @param name
     * @param email
     */
    public void fetchUser(String respMsg, String name, String email) {
        DatabaseReference ref = database.getReference("users");
        ref.addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int accepted = 0;
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    User user = snap.getValue(User.class);
                    if (user.getName().equals(name) && user.getEmail().equals(email)) {
                        Log.d(MainActivity.TAG,"<FIREBASE> FETCH user " + user.getName() + ":" + name + "-" + email);
                        delegate.getResponse(respMsg, user);
                        accepted++;
                    }
                }
                if (accepted == 0) delegate.getResponse(NO_USER_FOUND, null);
            }
        });
    }

    public void fetchGuestList(String respMsg, String eventId) {
        DatabaseReference ref = database.getReference("guest_lists");
        ref.child(eventId).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> guestList = new HashMap<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User g = snapshot.getValue(User.class);
                    guestList.put(g.getName(), g.getEmail());
                }
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH guest list for " + eventId + " with " + guestList.keySet().size() + " entries");
                delegate.getResponse(respMsg, guestList);
            }
        });
    }

    public void fetchEvents(String respMsg) {
        DatabaseReference ref = database.getReference("events");
        ref.addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Event> events = new HashMap<>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Event e = snap.getValue(Event.class);
                    events.put(snap.getKey(), e);
                }
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH events : " + events.keySet().size() + " entries");
                delegate.getResponse(respMsg, events);
            }
        });
    }

    public void fetchEvent(String respMsg, String eventId) {
        DatabaseReference ref = database.getReference("events");
        ref.child(eventId).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event e = dataSnapshot.getValue(Event.class);
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH event " + e.getName() + ":" + eventId);
                delegate.getResponse(respMsg, e);
            }
        });
    }

    public void fetchProjects(String respMsg, String eventId) {
        DatabaseReference ref = database.getReference("projects");
        ref.child(eventId).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Project> projects = new HashMap<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Project p = snapshot.getValue(Project.class);
                    projects.put(p.getProjectId(), p);
                }
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH projects for " + eventId + " with " + projects.keySet().size() + " entries");

                delegate.getResponse(respMsg, projects);
            }
        });
    }

    public void fetchAndValidate(String respMsg, String signOnFilter) {
        DatabaseReference ref = database.getReference();
        HashMap<String, Event> events = new HashMap<>();
        String updatedSignOn = "";
        ref.child("events").addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Event e = snap.getValue(Event.class);
                    events.put(e.getEventId(), e);
                }
            }
        });
        updatedSignOn = SecurityUtil.validateEventOwner(signOnFilter, events);
        for (String id : events.keySet()) {
            HashMap<String, Project> projects = new HashMap<>();
            ref.child("projects").child(id).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Project p = snapshot.getValue(Project.class);
                        projects.put(p.getProjectId(), p);
                    }
                }
            });
            updatedSignOn = SecurityUtil.validateProjectOwner(updatedSignOn,projects);
        }
        Log.d(MainActivity.TAG,"<FIREBASE> FETCH and validated");
        delegate.getResponse(respMsg,updatedSignOn);
    }

    @Override
    public void execute(String command) {

    }

    public void fetchProject(String respMsg, String eventId, String projectId) {
        DatabaseReference ref = database.getReference("projects");
        ref.child(eventId).addListenerForSingleValueEvent(new FetchFromFirebaseListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Project result = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Project p = snapshot.getValue(Project.class);
                    if(p.getProjectId().equals(projectId)){
                        result = p;
                    }
                }
                Log.d(MainActivity.TAG,"<FIREBASE> FETCH project for " + eventId + " and pId " + projectId);
                delegate.getResponse(respMsg, result);
            }
        });
    }

    public void putTimeline(String eventId, String projectId, String s, String dateString) {
        DatabaseReference ref = database.getReference("projects");
        ref.child(eventId).child(projectId).child("timeline").child(dateString).setValue(s);
        Log.d(MainActivity.TAG,"<FIREBASE> PUT timeline " + dateString + ":" + s);
    }
}
