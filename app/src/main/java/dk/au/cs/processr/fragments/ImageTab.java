package dk.au.cs.processr.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import dk.au.cs.processr.R;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.TabFragment;
import dk.au.cs.processr.utility.UtilityResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageTab extends android.support.v4.app.Fragment implements TabFragment, UtilityResponse{

    private FirebaseUtil firebase;

    public ImageTab() {

        firebase = new FirebaseUtil();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tab_fragment3, container, false);
    }

    @Override
    public void setContent(String eventId, String projectId) {
        firebase.delegate = this;
        firebase.fetchProject(FirebaseUtil.PROJECT,eventId,projectId);

    }

    @Override
    public void deleteContent() {
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if(returnMsg.equals(FirebaseUtil.PROJECT)){
            updateUI((Project)result);
        }
    }

    private void updateUI(Project result) {
    }
}