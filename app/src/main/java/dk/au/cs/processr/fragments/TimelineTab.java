package dk.au.cs.processr.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import dk.au.cs.processr.R;
import dk.au.cs.processr.serializable.Project;
import dk.au.cs.processr.utility.FirebaseUtil;
import dk.au.cs.processr.utility.TabFragment;
import dk.au.cs.processr.utility.UtilityResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineTab extends android.support.v4.app.Fragment implements TabFragment, UtilityResponse{

    private FirebaseUtil firebase;
    private String eventId;
    private String projectId;

    public TimelineTab() {

        firebase = new FirebaseUtil();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab_fragment2, container, false);
        view.findViewById(R.id.add_to_timeline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimeline(view);
            }
        });
        return view;
    }

    private void updateTimeline(View view) {
        Button add = view.findViewById(R.id.add_to_timeline);
        add.setVisibility(View.GONE);
        EditText text = view.findViewById(R.id.timeline_update_text);
        text.setVisibility(View.VISIBLE);
        Button push = view.findViewById(R.id.push_timeline);
        push.setVisibility(View.VISIBLE);
        push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss", Locale.UK);
                String dateString = format.format(date);
                firebase.putTimeline(eventId,projectId,text.getText().toString(),dateString);
                add.setVisibility(View.VISIBLE);
                push.setVisibility(View.GONE);
                text.setVisibility(View.GONE);
                text.setText("");
                deleteContent();
                setContent(eventId,projectId);
            }
        });

    }

    @Override
    public void setContent(String eventId, String projectId) {
        this.eventId = eventId;
        this.projectId = projectId;
        firebase.delegate = this;
        firebase.fetchProject(FirebaseUtil.PROJECT,eventId,projectId);
    }

    @Override
    public void getResponse(String returnMsg, Object result) {
        if(returnMsg.equals(FirebaseUtil.PROJECT)){
            updateUI((Project)result);
        }
}

    private void updateUI(Project result) {

        View view = getView();
        LinearLayout timelineId = (LinearLayout)view.findViewById(R.id.timeline);
        HashMap<String,String> posts =result.getTimeline();
        if (posts != null) {
            for (String key : posts.keySet()) {
                LinearLayout timeline = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.timeline_item,null);
                TextView date = new TextView(this.getContext());
                date.setText(key);
                ((TextView)timeline.findViewById(R.id.date)).setText(getStringFromDate(key));
                ((TextView)timeline.findViewById(R.id.update)).setText(posts.get(key));
                timelineId.addView(timeline);
            }
        }
    }

    private String getStringFromDate(String s) {
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss", Locale.UK);
        Date date = new Date();
        try {
            date = parser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date thisDate = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.UK);

        return String.format(Locale.UK,"%d",(long)((thisDate.getTime()-date.getTime())*0.000000277778)) + " hours ago \n" + formatter.format(date);
    }

    @Override
    public void deleteContent() {
        delete();
    }

    private void delete() {
        View view = getView();
        LinearLayout timeline =(LinearLayout)view.findViewById(R.id.timeline);
        timeline.removeAllViewsInLayout();
    }
}
