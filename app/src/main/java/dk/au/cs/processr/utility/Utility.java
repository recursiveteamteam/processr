package dk.au.cs.processr.utility;

/**
 * Created by Tobias Lund Petersen on 25-11-2017.
 */

public interface Utility {
    public void execute(String command);
}
