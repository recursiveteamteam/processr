# README #

A Readme for Processr, an event-, group- and process management android-app.
NOTE; This app is made as a project of the course Interactive Systems at Aarhus University.
The app is not released as an official app on Android, and this repo is for studying/viewing our code.

We're two people behind, Tobias Lund Petersen and Jesper Libach. 

### What is this repository for? ###

This repository is the source-code for the android app Processr. 
Key features of Processr are:

* NFC tag read/write, for assignment of specific projects
* Creating events, which hold projects in it
* Updating project-status through the timeline on a project

Version 1.0.0 (the app is not launched, and this does not reference any prior versions)

### How do I get set up? ###


> This App won't run on a virtual device, as NFC tags should be scanned by native NFC-enabled devices.

** General setup **
This app is usin normal Gradle and Android Studio setup, hence you'll just need to fork it to be able to run it.
The NPK/APK will be available shortly.

** NFC **
You'll need to have an NFC-tag of type MifareClassic, with 1K 16-byte memory.

To write to an NFC-tag, you should go to a project, click on the green button in the Taskbar, then hold the NFC-tag to your device.
As NFC-tags are not supposed to be written to very often, you might need to write to it (with the same project) one or two times more.

Then, from the main screen, you'll be able to go to the project-page through holding an NFC-tag to the device.


### Contribution guidelines ###

You're allowed to fork the project, but if you wan't to contribute directly to this repository, you can send an e-mail to us, and we'll look into it.

### Who do I talk to? ###

We're just two people,
Tobias Lund Petersen (tobias.lund1996@gmail.com)
Jesper Libach